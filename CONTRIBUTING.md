# Contributions

Contributions are welcome, especially for new binary types. This document
outlines the high-level components involved in adding new information.

The driving framework for the application is the [svelte](https://svelte.dev/)
framework due to speed and size. Additionally, _svelte_ enables new developers
to effectively contribute. The [svelte
tutorial](https://svelte.dev/tutorial/basics) is a thourough introduction to
working with the framework.

[src/App.svelte](src/App.svelte) is the _main_ of the application. New binary
types may be added in the `buildByteIntervals()` function. The majority of the
work is performed by the excellent [Kaitai](https://kaitai.io) parsers; they handle
translating a binary into a structured display of data.

To ensure maximum flexibility in display formats, the renderer works on the
concept of _ByteRanges_. A byte range is an inclusive range of bytes that has
structured data associated with it. A self-contained example of generating such
a range is available in `generateSymbolTableEntries()`. The Map enables
structured data to be displayed to the user.
