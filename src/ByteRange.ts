
/** Contains information about a range of bytes */
export class ByteRange {
    startInterval   : number;
    endInterval     : number;
    details       : Map<string, string>;
    ranges          : ByteRange[];

    constructor(
        theStart : number,
        theEnd : number,
        theHover : Map<string, string>,
        theRanges : ByteRange[]){

        this.startInterval  = theStart;
        this.endInterval    = theEnd;
        this.details      = theHover;
        this.ranges         = theRanges;
    }
}

export class ByteInformation {
    bytes : Uint8Array;
    ranges: ByteRange[];

    constructor(theBytes : Uint8Array, theRanges : ByteRange[])
    {
        this.bytes = theBytes;
        this.ranges = theRanges;
    }
}