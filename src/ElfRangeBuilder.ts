// Both of these are used. They populate the global namespace so the application
// can use them here.
import * as KaitaiStreamImport from "kaitai-struct/KaitaiStream";
import * as ElfImport from "./Elf.js"

// Since Kaitai is ancient and uses UMD module systems that add data
// directly to the global namespace, alias those to something that is more
// real. Note that the TS compiler and intellisense will both lie
// and say this isn't working.
const Elf = window.Elf;
const KaitaiStream = window.KaitaiStream;

import 'svelte'
import {ByteRange, ByteInformation} from "./ByteRange";

export {};

let strtabOffset : number = -1;

function floatIsEqual(floatA : number, floatB : number) : boolean {
    return Math.abs(floatA - floatB) <= .0000001;
}

function sortRanges(a : ByteRange, b :ByteRange)
{
    if (a.startInterval < b.startInterval) {
        return -1;
    } else if (a.startInterval > b.startInterval) {
        return 1;
    }
    return 0;
}
function getNameAtOffset(theOffset : number,
        theByteCollection) : string {
    let result : string = "";
    if (-1 == strtabOffset) {
        return "(NO STRTAB)";
    }
    let stringStart : number = strtabOffset + theOffset;

    if (theOffset === 0 ||
        stringStart >= theByteCollection.length) {
        return "(NULL)";
    }

    for (let i = stringStart; i < theByteCollection.length; i++) {
        const byte : number = theByteCollection[i];
        if (0 === byte) {
            break;
        }
        result = result + String.fromCharCode(byte);
    }

    return result;
}

function generateSymbolTableEntries(theSection, theResultArray : ByteRange[],
    theByteCollection) {
    let entries = theSection.body.entries;
    for (let i = 0; i < entries.length; i++) {
        const element = entries[i];

        const nameOffset : number = element.ofsName;
        const name : string = getNameAtOffset(nameOffset, theByteCollection);
        const value = element.valueB64;
        const size = element.sizeB64;
        const startOffset = (i * theSection.entrySize)  + theSection.ofsBody;
        const endOffset = startOffset + theSection.entrySize;

        let map : Map<string, string> = new Map<string, string>();
        map.set("name",name);
        map.set("value",value);
        map.set("size",size);
        map.set("binding", Elf.SymbolBinding[element.bind]);
        map.set("type", Elf.SymbolType[element.type]);
        map.set("visibility", Elf.SymbolVisibility[element.visibility]);
        map.set("other", element.other);

        theResultArray.push(new ByteRange(
            startOffset,
            endOffset,
            map, null));
    }
}

function functionGenerateStrtabEntries(theSection, theResultArray : ByteRange[]) {
    console.log(theSection);
    const strings : string[] = theSection.body.entries;
    const offsetStart = theSection.ofsBody;
    const nullCounter = 1;

    let offset = 0;
    for (let i = 0; i < strings.length; i++) {
        const element = strings[i];
        theResultArray.push(new ByteRange(
            offsetStart + offset, (offsetStart + offset + element.length),
            new Map([["str", element]]), null));
        offset += nullCounter + element.length;
    }
}

function generateRangesForSection(theSection, theByteCollection) : ByteRange[] {
    let result : ByteRange[] = [];
    if (floatIsEqual(Elf.ShType.DYNSYM, theSection.type) ||
        floatIsEqual(Elf.ShType.SYMTAB, theSection.type)) {
        generateSymbolTableEntries(theSection, result, theByteCollection);
    } else if (floatIsEqual(Elf.ShType.STRTAB, theSection.type)) {
        functionGenerateStrtabEntries(theSection, result);
    }

    return result;
}

function getStrtabOffset(theElfParser) : number {
    let result :number = 0;
    for (let i = 0; i < theElfParser.header.sectionHeaders.length; i++) {
        const element = theElfParser.header.sectionHeaders[i];
        if (floatIsEqual(Elf.ShType.STRTAB, element.type) &&
            ".dynstr" === element.name) {
            return element.ofsBody;
        }
    }

    console.log("Failed to find offset for string table");

    return -1;
}

function buildSectionHeaderExpansion(
    theSection) : ByteRange
{
    let start : number = theSection.ofsBody;
    let end : number = Math.round(theSection.ofsBody + theSection.lenBody);

    let expansionMap : Map<string, string> = new Map<string, string>();
    expansionMap.set("name", theSection.name);
    expansionMap.set("type", Elf.ShType[theSection.type]);
    expansionMap.set("length", theSection.lenBody + " bytes");
    expansionMap.set("align", theSection.align);

    return new ByteRange(start, end, expansionMap, null);
}

function generateElfHeaderRange(theHeader) : ByteRange
{
    let start : number = 0;
    let end : number = theHeader.eEhsize;
    let details : Map<string,string> = new Map<string,string>();

    details.set("name", "ELF Header")
    details.set("Type", Elf.ObjType[theHeader.eType]);
    details.set("Entry", "0x" + theHeader.entryPoint.toString(16));
    details.set("Machine", Elf.Machine[theHeader.machine]);
    details.set("Program Headers", theHeader.qtyProgramHeader);
    details.set("Sections", theHeader.qtySectionHeader);
    details.set("Architecture size", Elf.Bits[theHeader._root.bits]);
    details.set("Endianness", Elf.Endian[theHeader._root.endian]);

    return new ByteRange(
        start,
        end,
        details,
        [new ByteRange(start, end, details, null)]);
}

export async function buildByteIntervalsELF(
    theByteCollection : Uint8Array,
    byteRangeCollection : ByteRange[]) : Promise<ByteInformation>
{
    let elfParser = new Elf(new KaitaiStream(theByteCollection));
    strtabOffset = getStrtabOffset(elfParser);

    console.log(elfParser, strtabOffset);
    byteRangeCollection = [];

    if (elfParser.header.qtyProgramHeader > 0) {
        byteRangeCollection.push(new ByteRange(
            elfParser.header.programHeaderOffset,
            elfParser.header.qtyProgramHeader * elfParser.header.programHeaderEntrySize,
            new Map([["name", "Program Headers"]]),
            null));
    }

    byteRangeCollection.push(generateElfHeaderRange(elfParser.header));
    let sectionHeaderExpansion : ByteRange[] = [];

    for (let index = 0; index < elfParser.header.sectionHeaders.length; index++) {
        const element = elfParser.header.sectionHeaders[index];
        if ((element.ofsBody === 0 && 0 === element.lenBody) ||
            element.ofsBody > theByteCollection.length) {
            continue;
        }

        sectionHeaderExpansion.push(buildSectionHeaderExpansion(element));

        let endOffset :number = Math.round(element.lenBody + element.ofsBody);
        if (floatIsEqual(Elf.ShType.NOBITS, element.type)) {
            endOffset = element.ofsBody;
        }

        byteRangeCollection.push(new ByteRange(
            element.ofsBody,
            endOffset,
            new Map([["name",element.name]]),
            generateRangesForSection(element, theByteCollection)));
    }

    byteRangeCollection.push(new ByteRange(
        elfParser.header.sectionHeaderOffset,
        Math.round(elfParser.header.qtySectionHeader * elfParser.header.sectionHeaderEntrySize),
        new Map([["name", "Section Headers"]]),
        sectionHeaderExpansion
    ));

    byteRangeCollection.sort(sortRanges);

    for (let i = 1; i < byteRangeCollection.length; i++) {
        let prev = byteRangeCollection[i-1];
        let current = byteRangeCollection[i];
        if ((current.startInterval - prev.endInterval) > 1) {
            byteRangeCollection.splice(i, 0, new ByteRange(
                prev.endInterval + 1,
                current.startInterval -1,
                new Map([["name", "DEAD DATA"]]),
                null));
            i--;
        }
    }

    // Trigger the updates
    return new ByteInformation(theByteCollection, byteRangeCollection);
}