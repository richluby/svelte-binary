# To Start

VIew in Firefox :

![Screenshot](data/screenshot.jpg)

## Locally

```
cd svelte-binary
npm install
npm run dev --host
```
Navigate to `127.0.0.1:5000`.

## Docker

```
cd svelte-binary
docker run --rm -itv $PWD:/work -w /work -p 127.0.0.1:5000:5000 --entrypoint /bin/bash node
npm install
npm run dev --host
```

Navigate to `127.0.0.1:5000`.

---

# Navigating the UI

The section names on the left provide a "jump-to" functionality. If the section
is recognized as a decodable section, then an additional column will appear
with the decoded data. The decoded data supports a "jump-to" functionality as
well.
